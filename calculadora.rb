require_relative 'operacoes_extras'
require 'net/http'
require 'json'

module Calculadora

  class Operacoes
    include OperacoesExtras

    def media_preconceituosa(notas, lista_negra)
      puts "teste media_preconceituosa"

    end

    def sem_numeros(numeros) 
      puts numeros
      x=numeros[-2..-1] #pega os 2 últimos caracteres da string
      
      if (x=="00") or (x=="25") or (x=="50") or (x=="75")
        puts "S" #O valor é divisível por 25
      else
        puts "N" #O valor não é divisível por 25
      end
    
    end

    def filtrar_filmes(generos, ano)
      filmes = get_filmes
      puts "filtrar_filmes"


    end
    
    private

    def get_filmes
      url = 'https://raw.githubusercontent.com/yegor-sytnyk/movies-list/master/db.json'
      uri = URI(url)
      response = Net::HTTP.get(uri)
      return JSON.parse(response, symbolize_names: true)
    end

  end

  class Menu
  
    def initialize
    end
  
  end

end

puts "------------------------------"
puts "| Bem vindo(a) à Calculadora |"
puts "------------------------------"
puts "\n"
puts "Escolha a operação que deseja realizar: "
puts "1 - Operação Média Preconceituosa"
puts "2 - Operação Calculadora sem números"
puts "3 - Operação Filtro de Filmes"
puts "0 - Sair"
print "Opção: "
    
option = gets.chomp.to_i

puts ""

obj=Calculadora::Operacoes.new
 
if option==1
  puts "Operação Média Preconceituosa"
  obj.media_preconceituosa()
 
 
elsif option==2
  puts "Operação Calculadora sem números"
  print "Digite o valor a ser analisado: "
  val= gets.chomp
  obj.sem_numeros(val)
  
  
    
elsif option==3
  puts "Operação Filtro de Filmes"
  obj.get_filmes
  
  
    
elsif option==0
  return 0  
    
   
else puts "Operação Inválida"
    
end